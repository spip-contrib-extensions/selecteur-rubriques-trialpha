<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/urls_par_numero/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// U
	'selecteur-rubriques-trialpha_description' => 'Modifie le sélecteur de rubriques pour afficher les rubriques par ordre alphabétique',
	'selecteur-rubriques-trialpha_slogan' => 'Pour mieux s\'y retrouver'
);

?>
